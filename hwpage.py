'''	@file		hwpage.py
	@brief		Page describing the closed loop system model, its tuning, and the simulation results.

	\page	hw	Homework 2 and 3: Ball Balancing Platform Model and Simulation
	@htmlinclude 	Hw2_3_BalancingPlatformModel_Simulation.html
'''