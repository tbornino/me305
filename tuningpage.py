''' @file                tuningpage.py
    @brief               Page describing the closed loop system and its tuning.
    
    \page tuning Closed Loop Control
    
    We tuned the proportional velocity controller that we implemented in lab 4.
    A block diagram for this controller and how it interfaces with the rest
    of our code and with the physical system is below:
    \image html Lab4BlockDiagram.jpg
    
    We use a cooperative multitasking system to run our tasks. So far, we have
        1.  An "encoder task" to update the encoder positions and changes in position or "deltas"
        2.  A "user task" to take input from the user and run tests on the motor,
            encoders, and closed loop controller.
        3.  A "motor task" to apply the desired PWM duty cycle to the motors
        4.  A "controller task" task to take data from the encoder and apply proportional
            feedback control.
    
    The task diagram for our system is below:
    \image html Lab4taskdiagram.jpg
    
    Our tasks are each implemented as Finite State Machines (FSMs).
    These are displayed below as state transition diagrams.
    \image html Lab2taskencoderFSM.jpg "Encoder task FSM"
    \image html Lab4taskuserFSM.jpg "User Task FSM"
    \image html Lab3taskmotorFSM.jpg "Motor Task FSM"
    \image html Lab4taskcontrollerFSM.jpg

    The results of our tuning are summarized in these graphs. As we increased the
    propotional gain, our steady state error decreased, but some oscillations
    occured before settling to the steady state value.
    \image html Kptuneplot01.png
    \image html Kptuneplot05.png
    \image html Kptuneplot08.png
    \image html Kptuneplot10.png
    \image html Kptuneplot12.png
    \image html Kptuneplotall.png
'''