''' @file       task_panel.py
    @brief      A task for the touch panel to collect linear data.
    @details    Contains the class Task_Panel.          
    @author     Tori Bornino
    @author     Jackson McLaughlin
    @author     Kendall Chappell
    @date       December 7, 2021
'''
import touchpanel
import utime
import os
#from pyb import Pin, ADC
import pyb
#import micropython
from ulab import numpy as np
#from ulab.scipy import linalg

#_MARGIN = micropython.const(10)
#_IN = micropython.const(Pin.IN)
#_OUT = micropython.const(Pin.OUT_PP)

class Task_Panel:
    ''' @brief      A class for calibrating and passing data readings from the touch panel.
        @details    A task for calibrating and collecting pressure location and velocity
                    readings from the touch panel in the x and y directions. The data is read
                    from the panel driver and shared as a shares object with the controller
                    task and the data collection task. 
                    
    '''
    
    def __init__(self, PANEL_PERIOD,  panel_share):
        ''' @brief      Initializes a panel task.
            @details    This will instantiate a touch panel object which interfaces with 
                        the touch panel driver, the data collection task, and the controller 
                        task. As touch panel data is collected, it is sharedd with the appropriate
                        taks. 
            @param      PANEL_PERIOD   Period, in microseconds, at which to check for 
                                       touch panel readings.
            @param      panel_share    Share object for sharing data read from the touch panel. 
        '''
        self._PANEL_PERIOD = PANEL_PERIOD
        self._nextTimePanelRead = utime.ticks_add(utime.ticks_us(), self._PANEL_PERIOD)
        
        self._panel_share = panel_share
        self._panel = touchpanel.TouchPanel(t_s=self._PANEL_PERIOD / 1e6) # PANEL_PERIOD in ms
        
    def run(self):
        '''@brief       Run the panel task.
           @details     Checks if it's time for the panel to run. If it is time to run, 
                        the task collects filtered data from the touch panel of the ball 
                        position and velocity (x, xdot, y, ydot) in (mm, mm/s, mm, mm/s). 
                        The data is written as a shares object. 
        '''
        
        if utime.ticks_diff(utime.ticks_us(), self._nextTimePanelRead) >= 0:
            self._nextTimePanelRead = utime.ticks_add(self._nextTimePanelRead, self._PANEL_PERIOD)
        # base operation
            reading = self._panel.get_AB_filtered()
        
#            if self.printcount == 50:
#                print("panel: ", reading)
#                self.printcount = 0
#            else:
#                self.printcount += 1
            self._panel_share.write(reading)

    def calibrate(self):
        '''@brief       Calibrates the touch panel.
           @details     Checks if the pyb flash contains a file of the calibration
                        coefficients to determine if the panel has already been calibrated.
                        If it has not been calibrated, the user must calibrate the touch panel. 
                        The task asks the user to press a series of locations: enter confirms
                        the position has been touched, backspace requests to redo the most recent point
                        calibration. The task recognizes that the IMU has been fully calibrated and writes
                        the calibration coefficinets to a text file to be stored on the console.  
        '''
        serport = pyb.USB_VCP()                                     
        filename = "RT_cal_coeffs.txt"
        read_points = np.zeros((9, 3))
        if filename in os.listdir():
            print('Found panel calibration file. values = ')
            with open(filename, 'r') as f:
                cal_data_string = f.readline()
            print(cal_data_string)
            cal_values = [float(cal_value) for cal_value in cal_data_string.strip().split(',')]
            self._panel.set_calibration(cal_values)
        else:
            for i in range(9):
                print("Press point ({}, {})".format(
                        self._panel._actual_points[i,0],
                        self._panel._actual_points[i,1]))
                while True:
                    scan_count = 0
                    ADC_x_list = []
                    ADC_y_list = []
                    while scan_count < 10:
                        ADC_x, ADC_y, z = self._panel.scanAllRaw()
                        if z:
                            ADC_x_list.append(ADC_x)
                            ADC_y_list.append(ADC_y)
                            scan_count += 1
                    ADC_x = sum(ADC_x_list) / 10
                    ADC_y = sum(ADC_y_list) / 10
                    print("You pressed ({}, {}) (ADC reading). Enter to confirm, backspace to try again".format(ADC_x, ADC_y))
                    validEntry = False
                    discard = False
                    while not validEntry:
                        if serport.any():
                            key = serport.read(1).decode()            
                            if key == '\x7F': # Backspace
                                validEntry = True
                                discard = True
                                print("Canceled. Press Point ({}, {})".format(
                                        self._panel._actual_points[i,0],
                                        self._panel._actual_points[i,1]))
                            elif key == '\r' or key == '\n':
                                validEntry = True
                                discard = False
                            else:
                                print("not valid entry")
                                continue
                    
                    if discard:
                        continue
                    else:
                        read_points[i,0] = ADC_x
                        read_points[i,1] = ADC_y
                        read_points[i,2] = 1
                        break
            print("Finished Plate Calibration. values = ")
            Kxx, Kxy, Kyx, Kyy, Xc, Yc = self._panel.calibrate(read_points)    # Do calc
            # Save coeff
            cal_data_string = f"{Kxx}, {Kxy}, {Kyx}, {Kyy}, {Xc}, {Yc}\r\n"
            print(cal_data_string)
            with open(filename, 'w') as f:
                f.write(cal_data_string)