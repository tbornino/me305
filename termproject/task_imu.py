''' @file       task_imu.py
    @brief      A task for the imu to collect angular data.
    @details    A task for collecting the calibration status,
                calibration coefficients, Euler angles and angular 
                velocity from the IMU - BNO055. The task writes the 
                Euler angles and angular velocity as a tuple 
                to a shares object in the form 
                (theta_y, theta_y_dot, theta_x, theta_x_dot).
    @author     Tori Bornino
    @author     Jackson McLaughlin
    @author     Kendall Chappell
    @date       November 6, 2021
'''

import BNO055
import os
import pyb
import utime

# Operating Modes
_CONFIGMODE = 0b0000
_NDOF = 0b1100

class Task_IMU:
    ''' @brief      
        @details    
    '''
    
    def __init__(self, IMU_PERIOD,  imu_share):
        ''' @brief      Initializes an IMU task
            @details    This will instantiate an IMU object which calibrates and
                        reads from the IMU - BNO055, then shares the readings to 
                        the controller task and a data collection state. 
                        
            @param      IMU_PERIOD   Period, in microseconds,that the IMU takes readings.
            @param      imu_share    Share object for sharing data read from the IMU
        '''
        self._periodIMU = IMU_PERIOD
        self._nextTimeIMURead = utime.ticks_add(utime.ticks_us(), self._periodIMU)
        
        self._imu_share = imu_share
        
        i2cController = pyb.I2C(1, pyb.I2C.CONTROLLER)
        self._imu = BNO055.BNO055(i2cController)

    def run(self):
        '''@brief       Runs the imu task.
           @details     Checks if it's time to run, then reads the angles and
                        angular velocity from the IMU. The readings are stored
                        as a tuple in the form (theta_y, theta_y_dot, theta_x, theta_x_dot).
        '''
        if utime.ticks_diff(utime.ticks_us(), self._nextTimeIMURead) >= 0:
            self._nextTimeIMURead = utime.ticks_add(self._nextTimeIMURead, self._periodIMU)
            # base operation
            angles = self._imu.read_euler_angles()
            angularVelocity = self._imu.read_angular_velocity()
            reading = (angles[2], angularVelocity[2], angles[1], angularVelocity[1])
            self._imu_share.write(reading)

    def calibrate(self):
        # probably needs to be changed some
        '''@brief       Runs the IMU calbration.
           @details     Checks if the pyb flash contains a file of the calibration
                        coefficients to determine if the IMU has already been calibrated.
                        If it has not been calibrated, the user must calibrate 
                        the magnetometer, accelerometer, and gyroscope of the IMU. The 
                        task recognizes that the IMU has been fully calibrated and writes
                        the calibration coefficinets to a text file to be stored on the console. 
        '''
        # Initialize IMU
        
        filename = 'IMU_cal_coeffs.txt'
        # If calibration data exists, load it
        if filename in os.listdir() :
            print('Found IMU calibration file. values = ')
            # importing Calibration Coefficients
            self._imu.set_operating_mode(_CONFIGMODE)
            with open(filename, 'r') as f:
                calib_string = f.readline().strip()
            print(calib_string)
            coeffs = bytes([int(coeff) for coeff in calib_string.split(',')])
            self._imu.set_calibration_coefficients(coeffs)
            self._imu.set_operating_mode(_NDOF)
            
        # If no existing calibration data, calibrate IMU and save calibration data
        else:
            # initial Calibration
            self._imu.set_operating_mode(_NDOF)
            status = self._imu.get_calibration_status()[0]
            print("Calibrating IMU...")
            magCali = False
            accCali = False
            gyrCali = False
            while status != 0b11111111:
                if ((status & 0b11) == 0b11) and not magCali:
                    print("magnetometer calibrated")
                    magCali = True
                if ((status & 0b1100) == 0b1100) and not accCali:
                    print("accelerometer calibrated")
                    accCali = True
                if ((status & 0b110000) == 0b110000) and not gyrCali:
                    print("gyroscope calibrated")
                    gyrCali = True
                status = self._imu.get_calibration_status()[0]
            print("fully calibrated IMU. values = ")
            
            # print calibration data to file
            calibration = self._imu.get_calibration_coefficients()
            templist = []
            for x in calibration:
                templist.append(hex(x))
            calib_string = ','.join(templist)
            print(calib_string)
            with open('IMU_cal_coeffs.txt', 'w') as f:
                f.write(calib_string)