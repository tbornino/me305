''' @file                       task_controller.py
    @brief                      A task for updating an encoder
    @details                    Contains the class Task_User.
                                See state diagram below.
                                \image html Lab3usertaskFSM.jpg
    @author                     Tori Bornino
    @author                     Jackson McLaughlin
    @author                     Kendall Chappell
    @date                       December 4, 2021
'''

##import statements
import DRV8847
import utime
from ulab import numpy as np
import math

# constants for positions of data in tuples
_X = 0
_XDOT = 1
_THETA_Y = 0
_THETA_YDOT = 1

_Y = 2
_YDOT = 3
_THETA_X = 2
_THETA_XDOT = 3
_Z = 4


#_kx = np.array([[1, -0.9, .3, -.12]]) # Lab Kit 05
#_ky = np.array([[-4.5, -1.9, -.3, .12]]) # Lab Kit 05

_k = np.array([[-2, -.5, -0.5, -.05]])
# Ky -4.5, -1.9, -.3, .12  Kx 4.5, -1.9, .3, -.12


#_ky = np.array([[-4.5,   -2.3,   -0.001,  -0.006]]) # Lab Kit 05

#_kx_p = np.array([[0,   -.98,   0,  -0.001]]) # Lab Kit 05
#_ky_p = np.array([[0,   -1,   0,  -0.005]]) # Lab Kit 05

#k = np.array([[-6,   -1,   -.001,   -0.005]])
#k = np.array([[0, 0, 0, -100]])


#from data sheet
#Terminal Resisitance = 2.21 Ohms
#Torque constant = 13.8 mNm/A
#Nominal voltage = 18V

_V_bc = 12 # Volts
_R = 2.21    #Ohms
_Kt = 13.8/1000  #Nm/A

_DutyConst = 100*_R/(4*_Kt*_V_bc)

_angConv = 2*math.pi/360


class Task_Controller:
     ''' @brief     A task for running a closed loop controller and controlling motors.  
         @details   This class is a task for running a closed loop state feedback
                    controller to compute and then set the motor duty cycle. 
                    It uses two shares to read IMU and panel data. The data 
                    is used to calculate the duty cycle for each motor by 
                    multiplying by vector gains.
                    
                    We tested the ability to collect data from the motors, 
                    and plotted the resulting position and delta values.
                    Motor 1 was run forwards at 50 percent duty cycle, and
                    Motor 2 was run backwards at 50 percent duty cycle.
                    \image html lab3plot1.png
                    \image html lab3plot2.jpg
     '''
    
     def __init__(self,CONTROLLER_PERIOD, balanceCMD, imu_share, panel_share, motor_share):
        '''@brief       Initialize a controller task
           @details     This will instantiate a closed loop state feedback object which 
                        collects data from the ball balancing platform and, 
                        when enabled, attempts to balance the ball on the platform.                        
           @param       periodController    Period, in microseconds, at which 
                                            the controller runs.
           @param       balanceCMD          Share indicating if the 
                                            controller_task should run.
           @param       imu_share           Share containing data collected by the imu
                                            stored in a tuple 
                                            (theta_y, theta_ydot, theta_x, theta_xdot)
                                            with units (deg, deg/s, deg, deg/s)
           @param       panel_share         Share containing data collected by the 
                                            resistive touch panel stored in a tuple 
                                            (x, xdot, y, ydot)
                                            with units (mm, mm/, mm, mm/s).
           @param       motor_share         Share containing the motor percent duty cycle  
                                            as a tuple (DutyX, DutyY).
        '''
        self._periodController = CONTROLLER_PERIOD
        self._nextTimeController = utime.ticks_add(utime.ticks_us(), self._periodController)
        
        self._balanceCMD = balanceCMD
        self._imu_share = imu_share
        self._panel_share = panel_share
        self._motor_share = motor_share
        
        self._motor_drv = DRV8847.DRV8847()
#        motor_drv.enable
        
        self._motorX = self._motor_drv.motor(1)
        self._motorY = self._motor_drv.motor(2)
        
     def run(self):
        '''@brief       Run the controller task.
           @details     If the balance command returns True, then data is read 
                        from the imu, panel, and motor shares and used to 
                        compute and set the motor duty cycle.
        '''
            
        if utime.ticks_diff(utime.ticks_us(), self._nextTimeController) >= 0:
            self._nextTimeController = utime.ticks_add(self._nextTimeController, self._periodController)
            
            anglesTemp = self._imu_share.read()
            positionTemp = self._panel_share.read()
            
            
            if self._balanceCMD.read():
                X1 = np.array((positionTemp[_X]/1000, anglesTemp[_THETA_Y]*_angConv, positionTemp[_XDOT]/1000, anglesTemp[_THETA_YDOT]*_angConv)).reshape((4,1))
                X2 = np.array((positionTemp[_Y]/1000, -anglesTemp[_THETA_X]*_angConv, positionTemp[_YDOT]/1000, -anglesTemp[_THETA_XDOT]*_angConv)).reshape((4,1))
                Tx = np.dot(-_k, X1)[0,0]
                Ty = np.dot(_k, X2)[0,0]
                
                if Tx >= 0:
                    Duty1 = min(_DutyConst * Tx, 100)
                else:
                    Duty1 = max(_DutyConst * Tx, -100)
                    
                if Ty >= 0:
                    Duty2 = min(_DutyConst * Ty, 100)
                else:
                    Duty2 = max(_DutyConst * Ty, -100)
                
#            if positionTemp[_Z] and self._balanceCMD.read():
#            
#                X1 = np.array((positionTemp[_X]/1000, anglesTemp[_THETA_Y]*_angConv, positionTemp[_XDOT]/1000, anglesTemp[_THETA_YDOT]*_angConv)).reshape((4,1))
#                X2 = np.array((positionTemp[_Y]/1000, -anglesTemp[_THETA_X]*_angConv, positionTemp[_YDOT]/1000, -anglesTemp[_THETA_XDOT]*_angConv)).reshape((4,1))
#
##                X1 = np.array((0, anglesTemp[THETA_Y]*angConv, 0, anglesTemp[THETA_YDOT]*angConv)).reshape((4,1))
##                X2 = np.array((0, anglesTemp[THETA_X]*angConv, 0, anglesTemp[THETA_XDOT]*angConv)).reshape((4,1))
##                
#                Tx = np.dot(_kx, X1)[0,0]
#                Ty = np.dot(_ky, X2)[0,0]
#                
#                if Tx >= 0:
#                    Duty1 = min(_DutyConst * Tx, 100)
#                else:
#                    Duty1 = max(_DutyConst * Tx, -100)
#                    
#                if Ty >= 0:
#                    Duty2 = min(_DutyConst * Ty, 100)
#                else:
#                    Duty2 = max(_DutyConst * Ty, -100)
#                    
#            elif not positionTemp[_Z] and self._balanceCMD.read():
#            
#
#                X1 = np.array((0, anglesTemp[_THETA_Y]*_angConv, 0, anglesTemp[_THETA_YDOT]*_angConv)).reshape((4,1))
#                X2 = np.array((0, -anglesTemp[_THETA_X]*_angConv, 0, anglesTemp[_THETA_XDOT]*_angConv)).reshape((4,1))
##                
#                Tx = np.dot(-_kx_p, X1)[0,0]
#                Ty = np.dot(_ky_p, X2)[0,0]
#                
#                if Tx >= 0:
#                    Duty1 = min(_DutyConst * Tx, 100)
#                else:
#                    Duty1 = max(_DutyConst * Tx, -100)
#                    
#                if Ty >= 0:
#                    Duty2 = min(_DutyConst * Ty, 100)
#                else:
#                    Duty2 = max(_DutyConst * Ty, -100)
            else:
                Duty1 = 0
                Duty2 = 0
                
            self._motorX.set_duty(Duty1)
            self._motorY.set_duty(Duty2)
            
            self._motor_share.write((Duty1, Duty2))
                
            #print('Duty1 = {}, Duty2 = {}'.format(Duty1, Duty2))
            
    