''' @file                       task_encoder.py
    @brief                      A task for updating an encoder.
    @details                    Includes the class Task_Encoder. See state
                                diagram below.
                                \image html Lab2encodertaskFSM.jpg
    @author                     Tori Bornino
    @author                     Jackson McLaughlin
    @date                       October 16, 2021
'''
import encoder
import pyb
import utime

class Task_Encoder2:
    ''' @brief                  A task for updating an encoder.
        @details                This class is a task for updating quadrature encoders.
                                It uses a Share to output its data to the rest
                                of the program.
    '''
    def __init__(self, period, zeroShare, dataShare, timerID=4, pin1=pyb.Pin.cpu.B6, pin2=pyb.Pin.cpu.B7):
        '''@brief      Initialize an encoder task.
           @details    This will instantiate an Encoder object which stores its
                       position and delta data in a Share. It sets up the task
                       to run at period.
           @param      period      Period, in microseconds, at which to take data from the encoder.
           @param      zeroShare   Share indicating whether encoders need to be zeroed.
           @param      dataShare   Share of encoder data as a tuple (time, position, delta).
           @param      timerID     Which timer on the Nucleo board to use (default 4).
           @param      pin1        First pin to connect the timer to (default B6).
           @param      pin2        Second pin to connect the timer to (default B7). 
        '''
        self.relevantEncoder = encoder.Encoder(timerID, pin1, pin2)
        self.period = period
        self.nextTime = utime.ticks_add(utime.ticks_us(), period)
        self.zeroShare = zeroShare
        self.dataShare = dataShare
             
    def run(self):
        '''@brief      Run the encoder task.
           @details    This will update the shared data (time, position, delta),
                       and update the encoder's position if necessary. See 
                       state diagram below.
                       \image html Lab2encodertaskFSM.jpg
        '''
        if utime.ticks_diff(utime.ticks_us(), self.nextTime) >= 0:
            self.nextTime = utime.ticks_add(self.nextTime, self.period)
            if (self.zeroShare.read()):
                self.relevantEncoder.set_position(0)
                self.zeroShare.write(False)
                
            self.relevantEncoder.update()
            self.dataShare.write((utime.ticks_ms(), self.relevantEncoder.get_position(), self.relevantEncoder.get_delta()))
