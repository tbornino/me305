''' @file                       task_user.py
    @brief                      A task for updating an encoder
    @details                    Contains the class Task_User.
                                See state diagram below.
                                \image html Lab2usertaskFSM.jpg
    @author                     Tori Bornino
    @author                     Jackson McLaughlin
    @date                       October 16, 2021
'''

timeCollect = 0
position = 1
delta = 2

WAIT = 0
COLLECT = 1
PRINT = 2

NO_COMMAND = 0

import pyb
import utime


class Task_User2:
    ''' @brief                  A task for user interaction with an encoder.
        @details                This class is a task for taking user input and
                                controlling an encoder display program.
                                It uses a Share to read encoder data, and
                                a Queue to store collected data when requested.
    '''
    def __init__(self, periodCheckCommands, periodGetData, zeroShare, dataShare, dataQueue):
        '''@brief       Initialize a user task
           @details     This will instantiate an User object which reads encoder
                        position and delta data from a Share. It sets up the read task
                        to run at periodCheckCommands. When user enters command
                        to record data, it sets up the store to run at 
                        periodGetData.
           @param       periodCheckCommands     Period, in microseconds, at which 
                                                to check for new user commands.
           @param       periodGetData           Period, in microseconds, at which 
                                                to record encoder data
           @param       zeroShare               Share indicating whether encoders need to be zeroed.    
           @param       dataShare               Share of encoder data as a tuple (time, position, delta)
           @param       dataQueue               Queue of encoder data as a tuple (time, position, delta) (used for printing)
        '''
        self.serport = pyb.USB_VCP()
        self.periodCheckCommands = periodCheckCommands
        self.nextTimeCheckCommands = utime.ticks_add(utime.ticks_us(), self.periodCheckCommands)
        self.periodGetData = periodGetData
        self.nexTimeGetData = int(0)
        self.getData = False
        self.dataLength = 30e6 / periodGetData + 1
        self.zeroShare = zeroShare
        self.dataShare = dataShare
        self.dataQueue = dataQueue
        self.command = NO_COMMAND
        self.state = WAIT
        
    
    def run(self):
        '''@brief       Run the user task.
           @details     Uses a finite state machine to interact with the user.
                        Commands are entered through characters at the serial
                        port. See list_commands() for command options.
                        See state diagram below.
                        \image html Lab2usertaskFSM.jpg
           @return      Flag indicating whether encoders need to be zeroed.
        '''
        
        if utime.ticks_diff(utime.ticks_us(), self.nextTimeCheckCommands) >= 0:
            if self.serport.any():
                self.command = self.serport.read(1)
            if self.state == WAIT:
                if self.command == b'z':
                    self.zeroShare.write(True)
                    print("zeroed position")
                    self.command = NO_COMMAND
                elif self.command == b'p':
                    print("position: ", self.dataShare.read()[position])
                    self.command = NO_COMMAND
                elif self.command == b'd':
                    print("delta: ", self.dataShare.read()[delta])
                    self.command = NO_COMMAND
                elif self.command == b'g':
                    self.nexTimeGetData = utime.ticks_add(utime.ticks_us(), self.periodGetData)
                    print("collecting data...")
                    self.command = NO_COMMAND
                    self.state = COLLECT
            
            elif self.state == COLLECT:
                if utime.ticks_diff(utime.ticks_us(), self.nexTimeGetData) >= 0:
                    self.dataQueue.put(self.dataShare.read())
                    self.nexTimeGetData = utime.ticks_add(self.nexTimeGetData, self.periodGetData)
                if self.dataQueue.num_in() == self.dataLength:
                    self.state = PRINT
                if self.command == b's':
                    print("data collection interrupted")
                    self.command = NO_COMMAND
                    self.state = PRINT
                    
            elif self.state == PRINT:
                print("Time,position,delta")
                for i in range(self.dataQueue.num_in()):
                    data = self.dataQueue.get()
                    if i == 0:
                        startTime = data[timeCollect]
                    dataTime = data[timeCollect]
                    pos = data[position]
                    d = data[delta]
                    print("{:},{:},{:}".format((utime.ticks_diff(dataTime, startTime) / 1000), pos, d))
                self.state = WAIT
               
            self.nextTimeCheckCommands = utime.ticks_add(self.nextTimeCheckCommands, self.periodCheckCommands)
    
    def list_commands(self):
        '''@brief       List the user commands available.
           @details     The available commands are:
                            - 'z': zero position
                            - 'p': print position
                            - 'd': print change in postion
                            - 'g': collect 30 seconds of position and delta data
                            - 's': end data collection early
        '''
        print("Available commands:")
        print("'z': zero position")
        print("'p': print position")
        print("'d': print change in postion")
        print("'g': collect 30 seconds of position and delta data")
        print("'s': end data collection early")