''' @file                       task_encoder.py
    @brief                      A task for updating an encoder.
    @details                    Includes the class Task_Encoder. See state
                                diagram below.
                                \image html Lab2encodertaskFSM.jpg
    @author                     Tori Bornino
    @author                     Jackson McLaughlin
    @date                       October 31, 2021
'''
import math
import encoder
import pyb
import utime

# Counts per revolution of our encoders
CPR = 4000.0

class Task_Encoder:
    ''' @brief                  A task for updating an encoder.
        @details                This class is a task for updating quadrature encoders.
                                It uses a Share to output its data to the rest
                                of the program.
    '''
    def __init__(self, period, zeroShare, dataShare, omegaMeasuredShare, encoderID):
        ''' @brief      Initialize an encoder task.
            @details    This will instantiate an Encoder object which stores its
                        position and delta data in a Share. It sets up the task
                        to run at period.
            @param      period              Period, in microseconds, at which to take data from the encoder.
            @param      zeroShare           Share indicating whether encoders need to be zeroed.
            @param      dataShare           Share of encoder data as a tuple (time, position, delta).
            @param      omegaMeasuredShare  Share to store measured velocity (rad/s)
            @param      encoderID           Which encoder to use (1 or 2). This selects 
                                            the pins and timers to use: timer 4 on pins
                                            B6 and B7 for encoder 1, and timer 8 on pins
                                            C6 and C7 for encoder 2.
            
        '''
        if encoderID == 1:
            self.relevantEncoder = encoder.Encoder(4, pyb.Pin.cpu.B6, pyb.Pin.cpu.B7)
        elif encoderID == 2:
            self.relevantEncoder = encoder.Encoder(8, pyb.Pin.cpu.C6, pyb.Pin.cpu.C7)
        else:
            raise ValueError("Invalid Motor ID. Motor ID should be 1 or 2.")
        
        
        self.period = period
        self.nextTime = utime.ticks_add(utime.ticks_us(), period)
        self.zeroShare = zeroShare
        self.dataShare = dataShare
        self.omegaMeasuredShare = omegaMeasuredShare
             
    def run(self):
        '''@brief      Run the encoder task.
           @details    This will update the shared data (time, position, delta),
                       and update the encoder's position if necessary. See 
                       state diagram below.
                       \image html Lab2encodertaskFSM.jpg
        '''
        if utime.ticks_diff(utime.ticks_us(), self.nextTime) >= 0:
            self.nextTime = utime.ticks_add(self.nextTime, self.period)
            if (self.zeroShare.read()):
                self.relevantEncoder.set_position(0)
                self.zeroShare.write(False)
                
            self.relevantEncoder.update()
            self.dataShare.write((utime.ticks_us(), self.relevantEncoder.get_position(), self.relevantEncoder.get_delta()))
            self.omegaMeasuredShare.write(self.relevantEncoder.get_delta() / (self.period/1000000) * 2.0 * math.pi / CPR)
