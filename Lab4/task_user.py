''' @file                       task_user.py
    @brief                      A task for updating an encoder
    @details                    Contains the class Task_User.
                                See state diagram below.
                                \image html Lab3usertaskFSM.jpg
    @author                     Tori Bornino
    @author                     Jackson McLaughlin
    @date                       October 31, 2021
'''
# constants for positions of data in tuples
TIME = 0
POSITION = 1
DELTA = 2
OMEGA = 1
ACTUATION = 2

# states
WAIT = 0
COLLECT_DATA = 1
PRINT_DATA = 2
STEP_RESPONSE = 3
PRINT_SR = 4
NUM_INPUT = 5

# constants for where to direct numerical inputs
C_DUTY1 = 0
C_DUTY2 = 1
C_KP1 = 2
C_KP2 = 3
C_OMEGA1 = 4
C_OMEGA2 = 5

# placeholder for when no command needs to be run
NO_COMMAND = 0

# Counts per revolution of our encoders
CPR = 4000

import pyb
import utime
import math


class Task_User:
    ''' @brief                  A task for user interaction with an encoder.
        @details                This class is a task for taking user input and
                                controlling an encoder display program.
                                It uses a Share to read encoder data, and
                                a Queue to store collected data when requested.

                                We tested the ability to collect data from the motors, 
                                and plotted the resulting position and delta values.
                                Motor 1 was run forwards at 50 percent duty cycle, and
                                Motor 2 was run backwards at 50 percent duty cycle.
                                \image html lab3plot1.png
                                \image html lab3plot2.jpg
    '''
    def __init__(self, periodCheckCommands, periodGetData, 
                 zeroShare1, zeroShare2,
                 dataShare1, dataShare2,
                 faultShare, 
                 motorShare1, motorShare2,
                 omegaDesiredShare1, omegaDesiredShare2,
                 omegaMeasuredShare1, omegaMeasuredShare2,
                 closedLoop1, closedLoop2,
                 dataQueue, srQueue):
        '''@brief       Initialize a user task
           @details     This will instantiate an User object which reads encoder
                        position and delta data from a Share. It sets up the read task
                        to run at periodCheckCommands. When user enters command
                        to record data, it sets up the store to run at 
                        periodGetData.
           @param       periodCheckCommands     Period, in microseconds, at which 
                                                to check for new user commands.
           @param       periodGetData           Period, in microseconds, at which 
                                                to record encoder data.
           @param       zeroShare1              Share indicating whether encoder 1 needs to be zeroed.
           @param       zeroShare2              Share indicating whether encoder 2 needs to be zeroed.
           @param       dataShare1              Share of encoder 1 data as a tuple (time, position, delta).
           @param       dataShare2              Share of encoder 2 data as a tuple (time, position, delta).
           @param       faultShare              Share holding whether a fault has been triggered and not cleared.
           @param       motorShare1             The share holding the duty cycle values to be set for motor 1.
           @param       motorShare2             The share holding the duty cycle values to be set for motor 2.
           @param       omegaDesiredShare1      Share to store desired motor 1 velocity (rad/s).
           @param       omegaDesiredShare2      Share to store desired motor 2 velocity (rad/s).
           @param       omegaMeasuredShare1     Share to store measured motor 1 velocity (rad/s).
           @param       omegaMeasuredShare2     Share to store measured motor 2 velocity (rad/s).
           @param       closedLoop1             Closed loop controller object for motor 1.
           @param       closedLoop2             Closed loop controller object for motor 2.
           @param       dataQueue               Queue of encoder data as a tuple (time, position, delta) (used for printing).
           @param       srQueue                 Queue for data from step response test.
        '''
        self.serport = pyb.USB_VCP()
        self.periodCheckCommands = periodCheckCommands
        self.nextTimeCheckCommands = utime.ticks_add(utime.ticks_us(), self.periodCheckCommands)
        self.periodGetData = periodGetData
        
        self.zeroShare1 = zeroShare1
        self.zeroShare2 = zeroShare2
        
        self.dataShare1 = dataShare1
        self.dataShare2 = dataShare2
            
        self.faultShare = faultShare
        
        self.motorShare1 = motorShare1
        self.motorShare2 = motorShare2
        
        self.omegaDesiredShare1 = omegaDesiredShare1
        self.omegaDesiredShare2 = omegaDesiredShare2
        
        self.omegaMeasuredShare1 = omegaMeasuredShare1
        self.omegaMeasuredShare2 = omegaMeasuredShare2
        
        self.closedLoop1 = closedLoop1
        self.closedLoop2 = closedLoop2
        
        self.dataQueue = dataQueue
        self.srQueue = srQueue
        
        self.command = NO_COMMAND
        self.state = WAIT
        
        self.dataLength = 30e6 / self.periodGetData + 1
        self.srLength = 10e6 / self.periodGetData + 1
        
        self.numString = ""
        
        self.duty = None
        self.Kp = None
        self.omega = None
        
    def run(self):
        '''@brief       Run the user task.
           @details     Uses a finite state machine to interact with the user.
                        Commands are entered through characters at the serial
                        port. See list_commands() for command options.
                        See state diagram below.
                        \image html Lab4taskuserFSM.jpg
        '''
        if self.state == WAIT:        
            
            # check if command
            if utime.ticks_diff(utime.ticks_us(), self.nextTimeCheckCommands) >= 0:
                if self.serport.any():
                    self.command = self.serport.read(1)
                self.nextTimeCheckCommands = utime.ticks_add(self.nextTimeCheckCommands, self.periodCheckCommands)
        
                if self.command == b'z':
                    self.zeroShare1.write(True)
                    print("zeroed encoder 1 position")
                    self.command = NO_COMMAND
                elif self.command == b'Z':
                    self.zeroShare2.write(True)
                    print("zeroed encoder 2 position")
                    self.command = NO_COMMAND
                elif self.command == b'p':
                    print("position of encoder 1: ", self.dataShare1.read()[POSITION])
                    self.command = NO_COMMAND
                elif self.command == b'P':
                    print("position of encoder 2: ", self.dataShare2.read()[POSITION])
                    self.command = NO_COMMAND
                elif self.command == b'd':
                    print("delta encoder 1: ", self.dataShare1.read()[DELTA])
                    self.command = NO_COMMAND
                elif self.command == b'D':
                    print("delta encoder 2: ", self.dataShare2.read()[DELTA])
                    self.command = NO_COMMAND
                elif self.command == b'm':
                    if self.duty == None:
                        print("What duty cycle would you like for motor 1?\nValid inputs between -100 and 100.")
                        self.inputID = C_DUTY1
                        self.state = NUM_INPUT
                    else:
                        self.motorShare1.write(self.duty)
                        self.duty = None
                        self.command = NO_COMMAND
                elif self.command == b'M':
                    if self.duty == None:
                        print("What duty cycle would you like for motor 2?\nValid inputs between -100 and 100.")
                        self.inputID = C_DUTY2
                        self.state = NUM_INPUT
                    else:
                        self.motorShare2.write(self.duty)
                        self.duty = None
                        self.command = NO_COMMAND
                elif self.command == b'g':
                    self.nexTimeGetData = utime.ticks_add(utime.ticks_us(), self.periodGetData)
                    print("collecting data on encoder 1...")
                    self.command = NO_COMMAND
                    self.state = COLLECT_DATA
                    self.collectID = 1
                elif self.command == b'G':
                    self.nexTimeGetData = utime.ticks_add(utime.ticks_us(), self.periodGetData)
                    print("collecting data on encoder 2...")
                    self.command = NO_COMMAND
                    self.state = COLLECT_DATA
                    self.collectID = 2
                elif self.command == b'c' or self.command == b'C':
                    print("Fault Cleared")
                    self.faultShare.write(False)
                    self.command = NO_COMMAND
                elif self.command == b'1':
                    if self.Kp == None:
                        print("Step response on motor 1")
                        print("What proportional gain Kp would you like (%duty/(rad/s)): ")
                        self.inputID = C_KP1
                        self.state = NUM_INPUT
                    elif self.omega == None:
                        print("What velocity would you like (rad/s): ")
                        self.inputID = C_OMEGA1
                        self.state = NUM_INPUT
                    else:
                        self.closedLoop1.set_Kp(self.Kp)
                        self.omegaDesiredShare1.write(self.omega)
                        self.Kp = None
                        self.omega = None
                        self.motorShare1.write(0)
                        self.collectID = 1
                        self.nexTimeGetData = utime.ticks_add(utime.ticks_us(), self.periodGetData)
                        self.closedLoop1.enable()
                        self.command = NO_COMMAND
                        self.state = STEP_RESPONSE                  
                elif self.command == b'2':
                    if self.Kp == None:
                        print("Step response on motor 1")
                        print("What proportional gain Kp would you like (%duty/(rad/s)): ")
                        self.inputID = C_KP2
                        self.state = NUM_INPUT
                    elif self.omega == None:
                        print("What velocity would you like (rad/s): ")
                        self.inputID = C_OMEGA2
                        self.state = NUM_INPUT
                    else:
                        self.closedLoop2.set_Kp(self.Kp)
                        self.omegaDesiredShare2.write(self.omega)
                        self.Kp = None
                        self.omega = None
                        self.motorShare2.write(0)
                        self.collectID = 2
                        self.nexTimeGetData = utime.ticks_add(utime.ticks_us(), self.periodGetData)
                        self.closedLoop2.enable()
                        self.command = NO_COMMAND
                        self.state = STEP_RESPONSE

        if self.state == COLLECT_DATA:
            
            # Check if cancel
            if utime.ticks_diff(utime.ticks_us(), self.nextTimeCheckCommands) >= 0:
                if self.serport.any():
                    self.command = self.serport.read(1)
                self.nextTimeCheckCommands = utime.ticks_add(self.nextTimeCheckCommands, self.periodCheckCommands)
                
                if self.command == b's' or self.command == b'S':
                    print("data collection interrupted")
                    self.command = NO_COMMAND
                    self.state = PRINT_DATA
                
            if utime.ticks_diff(utime.ticks_us(), self.nexTimeGetData) >= 0:
                
                if self.collectID == 1:
                    data = self.dataShare1.read()
                elif self.collectID == 2:
                    data = self.dataShare2.read()
                else:
                    raise ValueError("Invalid collect ID. Collect ID should be 1 or 2")
                
                t_ticks = data[TIME]
                
                pos_rad = data[POSITION] / CPR * 2 * math.pi
                
                if self.dataQueue.num_in() == 0:    
                    delta_rad_s = 0
                    self.last_pos = pos_rad
                    self.last_time = t_ticks  # not used first loop
                else:
                    delta_rad_s = (pos_rad - self.last_pos) / (utime.ticks_diff(t_ticks, self.last_time) / 1000000)
                    self.last_pos = pos_rad
                    self.last_time = t_ticks  # update for next round
                
                self.dataQueue.put((t_ticks, pos_rad, delta_rad_s))
                if self.dataQueue.num_in() == self.dataLength:
                    self.state = PRINT_DATA
                
                self.nexTimeGetData = utime.ticks_add(self.nexTimeGetData, self.periodGetData)
                
        if self.state == PRINT_DATA:
            print("Time,position,delta")
            for i in range(self.dataQueue.num_in()):
                data = self.dataQueue.get()
                if i == 0:
                    startTime = data[TIME]
                dataTime = data[TIME]
                pos = data[POSITION]
                d = data[DELTA]
                print("{:},{:},{:}".format(utime.ticks_diff(dataTime, startTime) / 1000000, pos, d))
            self.state = WAIT
        
        if self.state == STEP_RESPONSE:
            # Check if cancel
            if utime.ticks_diff(utime.ticks_us(), self.nextTimeCheckCommands) >= 0:
                if self.serport.any():
                    self.command = self.serport.read(1)
                self.nextTimeCheckCommands = utime.ticks_add(self.nextTimeCheckCommands, self.periodCheckCommands)
                if self.command == b's' or self.command == b'S':
                    print("step response interrupted")
                    self.closedLoop1.disable()
                    self.closedLoop2.disable()
                    self.command = NO_COMMAND
                    self.state = PRINT_SR
                
            # collects data during a step response
            if utime.ticks_diff(utime.ticks_us(), self.nexTimeGetData) >= 0:
                
                if self.collectID == 1:
                    t = self.dataShare1.read()[TIME]
                    omega = self.omegaMeasuredShare1.read()
                    actuation = self.motorShare1.read()
                    self.closedLoop1.enable()
                elif self.collectID == 2:
                    t = self.dataShare2.read()[TIME]
                    omega = self.omegaMeasuredShare2.read()
                    actuation = self.motorShare2.read()
                    self.closedLoop2.enable()
                else:
                    raise ValueError("Invalid collect ID. Collect ID should be 1 or 2")
                    
                self.srQueue.put((t, omega, actuation))
                if self.srQueue.num_in() == self.srLength:
                    self.closedLoop1.disable()
                    self.closedLoop2.disable()
                    self.state = PRINT_SR
                    
                self.nexTimeGetData = utime.ticks_add(self.nexTimeGetData, self.periodGetData)
                
        if self.state == PRINT_SR:
            # prints data collected during a step response
            print("Time,velocity,actuation level")
            for i in range(self.srQueue.num_in()):
                data = self.srQueue.get()
                if i == 0:
                    startTime = data[TIME]
                dataTime = data[TIME]
                omega = data[OMEGA]
                actuation = data[ACTUATION]
                print("{:},{:},{:}".format(utime.ticks_diff(dataTime, startTime) / 1000000, omega, actuation / 100))
            self.state = WAIT

        if self.state == NUM_INPUT:
            
            # Check if character is valid, then append to Number String
            if self.serport.any():
                nextChar = self.serport.read(1).decode()
                if nextChar.isdigit():
                    self.numString += nextChar
                    print('\r', self.numString, sep='', end='')
                elif nextChar == '-':
                    if self.numString == '':
                        self.numString += nextChar
                        print('\r', self.numString, sep='', end='')
                elif nextChar == '.':
                    if '.' not in self.numString:
                        self.numString += nextChar
                        print('\r', self.numString, sep='', end='')
                elif nextChar == '\x7F':
                    if self.numString != '':
                        self.numString = self.numString[:-1]
                        print('\r', self.numString, ' ', '\x7f', sep='', end='')
                        
                # when the user pressess enter, the number is assigned to correct variable
                elif (nextChar == '\r' or nextChar == '\n') and self.numString != '':
                    print()
                    if self.inputID == C_DUTY1 or self.inputID == C_DUTY2:
                        self.duty = float(self.numString)
                    elif self.inputID == C_KP1 or self.inputID == C_KP2:
                        self.Kp = float(self.numString)
                    elif self.inputID == C_OMEGA1 or self.inputID == C_OMEGA2:
                        self.omega = float(self.numString)
                    self.state = WAIT
                    self.numString = ''
            
            
            
    def list_commands(self):
        '''@brief       List the user commands available.
           @details     The available commands for encoder 1 are:
                            - 'z': zero position
                            - 'p': print position
                            - 'd': print change in postion
                            - 'g': collect 30 seconds of position and delta data
                            - 'm': enter duty cycle for motor 1
                            - '1': Step response test on motor 1
                        
                        The available commands for encoder 2 are:
                            - 'Z': zero position
                            - 'P': print position
                            - 'D': print change in postion
                            - 'G': collect 30 seconds of position and delta data
                            - 'M': enter duty cycle for motor 2
                            - '2': Step response test on motor 2
                        
                        The available commands for both encoders are:
                            - 's' or 'S': end data collection or step response early
                            - 'c' or 'C': clear fault condition
        '''
        print("The available commands for encoder 1 are:")
        print("    'z': zero position")
        print("    'p': print position")
        print("    'd': print change in postion")
        print("    'g': collect 30 seconds of position and delta data")
        print("    'm': enter duty cycle for motor 1")
        print("    '1': Step response test on motor 1")
        print("The available commands for encoder 2 are:")
        print("    'Z': zero position")
        print("    'P': print position")
        print("    'D': print change in postion")
        print("    'G': collect 30 seconds of position and delta data")
        print("    'M': enter duty cycle for motor 2")
        print("    '2': Step response test on motor 2")
        print("The available commands for both encoders are:")
        print("    's' or 'S': end data collection or step response early")
        print("    'c' or 'C': clear fault condition")