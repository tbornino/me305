''' @file           fibonacci.py
    @brief          Finds the fibbonacci number at the desired index
    @details        This file will calculate the fibonacci number at a user inputed index location.
    
    @author         Tori Bornino
    @date           September 26, 2021
'''



def fib(idx):
    #this function calculates the nth Fibonacci number when given and index n
    
    #This array holds the n-2, n-1 and n fibofibonacci numbers
    fibNum = [0, 1, 0] 
    if idx == fibNum[0]:
        fibNum[2] = fibNum[0] # if user asks for the 0th value
    elif idx == fibNum[1]: 
        fibNum[2] = fibNum[1] # if user asks for 1st value
    else:
        for n in range(idx-1): # if user asks for any other index
            # calculates nth term using n-2 and n-1 terms
            fibNum[2] = fibNum[0] + fibNum[1] 
            fibNum[0] = fibNum[1] # reassigns n-2 term
            fibNum[1] = fibNum[2] # reassigns n-1 term
            
    return fibNum[2]

def test(idx):
    #this function tests if user input is valid and 
    #on a valid response will rin our function
    
    
    if str.isdigit(idx) == False:
        print('I think we need to review what a positive integer is. \n'
              'Try a positive counting number such as 1, 2, ...')
    else:
        idx = int(idx) # change input to integer
        #for positive indicies calculaten desired number
        fib(idx) 
        #prints result
        print ('The Fibonacci number at index ' + str(idx) + 
               ' is ' + str(fib(idx))) 
   
    
if __name__ == '__main__':
    agn = 'Y'
    while agn == "Y" or agn == "y": 
        idx = input('Please enter a positive integer:') # asks user for index
        if len(idx) == 0: # if nothing is entered, print nothing and stop
            agn = 'n'
        else:
            test(idx) # runs funcitons
            #asks if user would like to continue
            agn = input('Would you like to go again? Y/N') 
        

    
    